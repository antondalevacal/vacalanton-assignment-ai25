<?php
require_once("User.php");
require_once("Todo.php");


//usage for user
$user = User::create([
    'first_name'    => 'John',
    'last_name'     => 'Doe',
    'age'           => 12,
    'address'       => 'Tacloban City'
]);

var_dump($user->get()); //Will display recently inserted record

$user = User::find(1);
$user->update([
    'first_name'    => 'Jose'
]);

var_dump($user->get());

$user = User::find(1);
$user->destroy();

//usage for todo

$user = User::create([
    'first_name'    => 'John',
    'last_name'     => 'Doe',
    'age'           => 12,
    'address'       => 'Tacloban City'
])->get();
$user_id = $user['user_id']
$todo = Todo::create([
    'user_id'       => $user_id,
    'description'   => 'Hello World',
]);

var_dump($todo->get()); //Will display recently inserted record

$todo = Todo::find(1);
$todo->update([
    'description'    => 'Hello Class'
]);

var_dump($todo->get());

$todo = Todo::find(1);
$todo->destroy();
?>
