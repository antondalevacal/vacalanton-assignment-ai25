<?php
class Db
{
    private static $instance = null;

    private $connection;


    private function __construct()
    {
        $servername = "localhost"; //change this
        $username = "root"; //change this
        $password = "root"; //change this

        $this->connection = new PDO("mysql:host=$servername;dbname=myDB", $username, $password);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function closeConnection()
    {
        $this->connection = null;
    }

    public static function connect()
    {
        if (!is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
?>
