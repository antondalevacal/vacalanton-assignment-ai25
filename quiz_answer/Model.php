<?php
require_once("Db.php");
require_once("ModelInterface.php");

abstract class Model implements ModelInterface
{
    protected $primary_key = "id";

    private $connection;
    private $selected_record_id;

    private function __construct($id = null)
    {
        $this->connetion = Db::connect()->getConnection();
        $this->selected_record_id = $id;
    }

    /**
     * Adds data to the table
     * Run insert query
     * @param Array $data
     * @return Object self
     */
    public static function create(Array $data)
    {
        $connection = Db::connect()->getConnection();

        $columns = implode(", ", array_keys($data));
        $values = implode(", ", array_map(function($e) { return "'" . $e . "'"; }, $data));

        $sql = "INSERT INTO " . $this->table . "(" . $columns . ") VALUES (" . $values . ")";
        $connection->exec($sql);

        return new self($connection->lastInsertId());
    }

    /**
     * Sets the instance of the model to whatever record you need
     * @param Integer $id
     * @return Object self
     */
    public static function find($id)
    {
        return new self($id);
    }

    /**
     * Deletes the record
     * @return void
     */
    public function destroy()
    {
        $query = $this->connection
            ->prepare("DELETE FROM ? WHERE ? = ?")
            ->bind_param($this->table, $this->primary_key, $this->selected_record_id)
            ->execute();
    }

    /**
     * Update selected record
     * @param Array $data
     * @return Object self
     */
    public function update(Array $data)
    {
        $sets = [];
        foreach ($data as $key => $value) {
            $sets = $key . "='" . $value . "'";
        }
        $sql = "UPDATE " . $this->table . " SET " . implode(", ", $sets) . " WHERE " . $this->primary_key . "=" . $this->selected_record_id;
        $this->conn->exec($sql);
        return $this;
    }

    /**
     * Get the record from the database based on selected ID
     * @return Array
     */
    public function get()
    {
        $query = $this->connection
            ->prepare("SELECT * FROM ? WHERE ? = ?")
            ->bind_param($this->table, $this->primary_key, $this->selected_record_id)
            ->execute();
        $data = $query->setFetchMode(PDO::FETCH_ASSOC);
        return new TableRows(new RecursiveArrayInterator($data)); //set the result into an associative array
    }
}
?>
