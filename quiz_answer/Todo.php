<?php
require_once("Model.php");

class Todo extends Model
{
    protected $table = "todos";
    protected $primary_key = "todo_id";
}
?>
