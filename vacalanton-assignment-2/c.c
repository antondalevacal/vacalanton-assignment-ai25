#include <stdio.h>

int main() {
   int age = 19;
   printf("(if/else-if/else)\n");
   if (age >= 18) {
	printf("Congrats! 18 kana!\n");
   }
   age = 99;
   if (age < 59) {
   	printf("Dire kapa maka avail hin senior citizen card.\n");
   }
   else if (age > 60) {
   	printf("Mano po lolo/lola.\n");
   }
   else {
   	printf("Kids kapala\n");
   }
   age = 9;
   if (age > 10) {
   	printf("Pwede kana mag internet hehe\n");
   }
   else {
   	printf("Manood kalang muna ng TV.\n");
   }

   printf("(while/do-while)\n");
   int y = 0;
   while (y < 5) {
   	printf("Copy-Paste lang sir.\n");
   	y++;
   }
   y = 0;
   do {
   	printf("Pansinin mo naman ako :<\n");
   	y++;
   } while (y < 5);
   
   printf("(for)\n");
   for (y = 0; y < 5; y++) {
   	printf("Every color, every hue..\n");
   }
   /*There is no foreach loop in C programming; you can just use for loops or any kind of loops to display items in your array.*/
}