class java {

    public static void main(String args[]){
	System.out.println("if/if-else-if/if-else");
	int age = 18;
    if(age >= 18) {
    	System.out.println("You are of legal age!");
    	}

    age = 15;
    if(age >= 18) {
    	System.out.println("You are of legal age!");
    }
    else if(age < 18) {
    	System.out.println("You are still underaged!");
    }
    else {
        System.out.println("You do not exist");
    }

	age = 12;
	if(age >= 18) {
		System.out.println("You can now vote!");
    }
    else {
		System.out.println("You aren't allowed to vote.");
    }

	System.out.println();

	System.out.println("while/do-while");
	int y = 0;
	while(y < 5) {
		System.out.println("Sana all programmer");
		y++;
	}
	System.out.println();

	y = 0;
	do {
		System.out.println("Sana all nakakacode");
		y++;
	} while (y < 5);

	System.out.println();

	System.out.println("for/for-each");
	for(y = 0;y < 5;y++) {
		System.out.println("China oil");
	}

	System.out.println();

	String[] names = { "Gillian", "Cecile", "Jamaica", "Roxanne", "Jalen"};
	for ( String num : names) {
		System.out.println(num);
	}
	}
}