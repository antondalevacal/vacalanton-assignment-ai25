<?php
	echo "<h2>if/else-if/else</h2>";
	$age = 18;
	if ($age >= 18) {
		echo "You are now 18!";
	}
	
	echo "<br>";

	$age = 15;
	if ($age >= 18) {
    	echo "You are of legal age";
    	}
    else if ($age < 18) {
    	echo "You are still underaged!";
    	}
	else {
		echo "Edi wow wala";
	}
	
	echo "<br>";
	
	$age = 12;
	if ($age >= 18) {
		echo "You are allowed to vote!";
    	}
    else {
		echo "You are not allowed to vote.";
    	}

	echo "<br>";

	echo "<h2>while/do-while</h2>";
	$y = 0;
	while ($y < 5) {
		echo "Sana all programmer.";
		echo "<br>";
		$y++;
	}

	echo "<br>";
	$y = 0;
	do {
		echo "Sana all nakakpaagcode.";
		echo "<br>";
		$y++;
	} while ($y < 5);

	echo "<br>";

	echo "<h2>for/for-each</h2>" ;
	for($y = 0;$y < 5;$y++) {
		echo "China Oil";
		echo "<br>";
	}

	echo "<br>";

	$games = array( "Dota 2", "League of Legends", "CS:GO", "Overwatch", "World of Warcraft");
	foreach ($games as $value) {
		echo "$value <br>";
	}		
?>