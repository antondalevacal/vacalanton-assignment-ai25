<?php
	$conn = new mysqli("localhost","root","","crud_vue");
	if ($conn->connect_error) {
		die("Connection Failed!".$conn->connect_error);
	}

	$result = array('error'=>false);
	$action = '';

	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	}

	if($action == 'read'){
		$sql = $conn->query("SELECT * FROM users");
		$users = array();
		while($row = $sql->fetch_assoc()){
			array_push($users, $row);
		}
		$result['users'] = $users;
	}

		if($action == 'create'){
		$name = $_POST['name'];
		$phone = $_POST['phone'];

		$sql = $conn->query("INSERT users (name,phone) VALUES('$name','$phone')");

		if($sql){
			$result['message'] = "Contact added successfully!";
		}
		else{
			$result['error'] = true;
			$result['message'] = "Failed to add contact!";
		}
	}

	if($action == 'update'){
		$id = $_POST['id'];
		$name = $_POST['name'];
		$phone = $_POST['phone'];

		$sql = $conn->query("UPDATE users SET name='$name', phone='$phone' WHERE id='$id'");

		if($sql){
			$result['message'] = "Contact updated successfully!";
		}
		else{
			$result['error'] = true;
			$result['message'] = "Failed to update contact!";
		}
	}

	if($action == 'delete'){
		$id = $_POST['id'];
		
		$sql = $conn->query("DELETE FROM users WHERE id='$id'");

		if($sql){
			$result['message'] = "Contact deleted successfully!";
		}
		else{
			$result['error'] = true;
			$result['message'] = "Failed to delete contact!";
		}
	}

	$conn->close();
	echo json_encode($result);
?>